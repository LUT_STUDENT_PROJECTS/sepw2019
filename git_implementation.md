## HOW TO GET GIT REPOSITORY CLONE TO YOUR OWN COMPUTER

1. Create account for bitbucket: [bitbucket.org](https://bitbucket.org/account/signup/)
2. Download client sw for your computer: [sourcetreeapp.com](https://www.sourcetreeapp.com/) (or any similar app) client v 3.1.3 is recommended for windows 10
3. Get invited to the repositoy by: `petri.tuominen@student.lut.fi`
4. Clone SEPW2019 repository for your own local disk with Sourcetree app
    + In new tab click remote, and under your account there should be SEPW2019 repo and clone button

    ![picture_name](help_for_git/clone_repo.PNG)

    + There is option to choose branch
    
    ![picture_name](help_for_git/clone_repo_advanced.PNG)

    + You can clone both of branches (master and development) to different folders on your local disk
5. Start working with your code and when some version is ok to publication, you can commit and push codes to the SEPW2019 repository with Sourcetree App

### Working with version control
+ `Remember always update (pull) all changes from online repository befor start editing your code`
+ `Remember always commit & push your changes to the online repository after you have done your changes `

### Working with branches - [Tutorial for Beginners #8 - Branches](https://www.youtube.com/watch?v=QV0kVNvkMxc)
+ You can have both of the cloned banches on your local disk
+ When you edit and testing new features, you can commit your changes to the development branch
+ When code is ready to publish, you can merge it to the master branch

### How to keep latest clone of repo in your local disk

1. "Fetch" from remote site (sepw2019/src/master)
2. "Pull" changes to your local disk
3. After this you can edit latest versions of files

### How to Push changes to the repository

1. In "File Status" tab you can see pending files
2. Add unstaged file to staged file
3. add comment which describing the change in the code
4. Commit & Push

### Public repository:
[https://bitbucket.org/LUT_STUDENT_PROJECTS/sepw2019/src/master/](https://bitbucket.org/LUT_STUDENT_PROJECTS/sepw2019/src/master/)

## Videos and manuals: 
### Best practices for git:
[github-best-practices](https://www.datree.io/resources/github-best-practices)

### Snapshots from the best practices:

```
# 6 – Don’t let secrets leak into source code
```

```
# 5 – Don’t commit dependencies into source code
```
