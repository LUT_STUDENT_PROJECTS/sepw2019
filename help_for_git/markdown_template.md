# Large header

Normal text

### Smaller header

Normal text

```
Normal text inside of gray box
```

`red text inside pink box`

    // Some comments
    Normal text
    Normal text
    Normal text

1. Normal text
2. Normal text
3. Normal text

## Medium header

+ Normal text `+`, `-`, or `*`
    - TNormal text:
        * Normal text
        + Normal text
        + Normal text
        - Normal text
+ Very easy!

| column 1 | column 2 |
| ------ | ----------- |
| row 1   | data field line 1. |
| row 2   | data field line 2. |
| row 3   | data field line 3. |

* **Bold text** - *in italics* - [header of the link](https://github.com/PurpleBooth)

### Embedded picture from the same folder
![picture_name](template_picture.PNG)



## Markdown information

* [Markdown demo](https://markdown-it.github.io/)

* [Markdown-Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)


